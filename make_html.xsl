<?xml version="1.0" encoding="UTF-8"?>
<!-- Given key list - reduce it for unique values -->
<xsl:stylesheet 
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"

	exclude-result-prefixes="xmlns"
>

<xsl:output method="xhtml" indent="no" omit-xml-declaration="yes" />

<xsl:template match="/">

	<xsl:element name="html">
		<xsl:element name="head">
			<xsl:element name="title">Album List</xsl:element>
		</xsl:element>
		<xsl:element name="body">
			<xsl:element name="h1">Album List</xsl:element>
			<xsl:element name="table">
				<xsl:attribute name="border">1</xsl:attribute>
				<xsl:element name="tr">
					<xsl:element name="th">Year</xsl:element>
					<xsl:element name="th">Album Artist</xsl:element>
					<xsl:element name="th">Album</xsl:element>
					<xsl:element name="th">Kind</xsl:element>
				</xsl:element>
				<xsl:for-each select="/Table/Row">
				<xsl:element name="tr">
					<xsl:element name="td"><xsl:value-of select="./Year"/></xsl:element>
					<xsl:element name="td"><xsl:value-of select="./Album_Artist"/></xsl:element>
					<xsl:element name="td"><xsl:value-of select="./Album"/></xsl:element>
					<xsl:element name="td"><xsl:value-of select="./Kind"/></xsl:element>
				</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:element>

</xsl:template>



</xsl:stylesheet> 
