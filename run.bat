SET SB_HOME=%cd%
SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_05
set CLASSPATH=lib\xmlparserv2.jar;lib\orai18n-collation.jar

if not exist "%SB_HOME%\output" mkdir "%SB_HOME%\output"

::del "%SB_HOME%\output\get_key_list.xml"
::"%JAVA_HOME%\bin\java" oracle.xml.parser.v2.oraxsl "%SB_HOME%\input\iTunes Music Library.xml" "%SB_HOME%\get_key_list.xsl" "%SB_HOME%\output\get_key_list.xml"

::**Key list was too big to reduce
::del "%SB_HOME%\output\reduce_key_list.xml"
::"%JAVA_HOME%\bin\java" oracle.xml.parser.v2.oraxsl "%SB_HOME%\output\get_key_list.xml" "%SB_HOME%\reduce_key_list.xsl" "%SB_HOME%\output\reduce_key_list.xml"


::Get initial list of tracks - includes criteria to reduce volume
del "%SB_HOME%\output\get_audio_track_list.xml"
"%JAVA_HOME%\bin\java" oracle.xml.parser.v2.oraxsl "%SB_HOME%\input\iTunes Music Library.xml" "%SB_HOME%\get_audio_track_list.xsl" "%SB_HOME%\output\get_audio_track_list.xml"

::Create a list of just albums from the audio track list
del "%SB_HOME%\output\reduce_to_album_list.xml"
"%JAVA_HOME%\bin\java" oracle.xml.parser.v2.oraxsl "%SB_HOME%\output\get_audio_track_list.xml" "%SB_HOME%\reduce_to_album_list.xsl" "%SB_HOME%\output\reduce_to_album_list.xml"

::Create html list of just albums from the audio track list
del "%SB_HOME%\output\album_list.html"
"%JAVA_HOME%\bin\java" oracle.xml.parser.v2.oraxsl "%SB_HOME%\output\reduce_to_album_list.xml" "%SB_HOME%\make_html.xsl" "%SB_HOME%\output\album_list.html"

pause
