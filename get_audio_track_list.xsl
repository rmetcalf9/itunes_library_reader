<?xml version="1.0" encoding="UTF-8"?>
<!-- Gest list of keys against tracks-->
<xsl:stylesheet 
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"

	exclude-result-prefixes="xmlns"
>

<!--<xsl:output method="text" indent="no" omit-xml-declaration="yes" />-->

<xsl:template match="/">

	<!-- Make sure we look only at the dict element following the tracks key -->
	<xsl:for-each select="/plist/dict/key[text()='Tracks']/following-sibling::dict[1]">

		<xsl:element name="Table">

			<xsl:for-each select="./dict">

<xsl:if test="count(./key[text()='Movie'])=0">
<xsl:if test="count(./key[text()='Has Video'])=0">
<xsl:variable name="curKind" select="./key[text()='Kind']/following-sibling::string[1]/text()"/>
<xsl:if test="contains($curKind,'iPad app')=false()">
<xsl:if test="contains($curKind,'iPod touch app')=false()">
<xsl:if test="contains($curKind,'Ringtone')=false()">
<xsl:if test="contains($curKind,'Protected book')=false()">
<xsl:if test="contains($curKind,'Purchased book')=false()">

				
				<xsl:element name="Row">

<!--<xsl:element name="Kind"><xsl:value-of select="$curKind"/></xsl:element>-->
				
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Year']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Release Date']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Track Type']"/></xsl:call-template>

<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Name']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Artist']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Album Artist']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Composer']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Album']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Genre']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Kind']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Sort Name']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Sort Album']"/></xsl:call-template>
<xsl:call-template name="outputField"><xsl:with-param name="key" select="./key[text()='Sort Artist']"/></xsl:call-template>

				</xsl:element>

</xsl:if>
</xsl:if>
</xsl:if>
</xsl:if>
</xsl:if>
</xsl:if>
</xsl:if>
				

			</xsl:for-each>


		</xsl:element>
	

	</xsl:for-each>
<xsl:text>
</xsl:text>
</xsl:template>

<xsl:template name="outputField">
	<xsl:param name="key"/>
	
	<!--Create a key name replacing space with underscore -->
	<xsl:variable name="keyName"><xsl:call-template name="replace-string">
		  <xsl:with-param name="text" select="$key/text()"/>
		  <xsl:with-param name="replace" select="' '" />
		  <xsl:with-param name="with" select="'_'"/>
		</xsl:call-template></xsl:variable>
	
	<xsl:if test="$keyName!=''">
		<xsl:element name="{$keyName}">
			<xsl:value-of select="$key/following-sibling::*[1]"/>
		</xsl:element>
	</xsl:if>
</xsl:template>

<!--Will only replace a max of 3 occurances-->
<xsl:template name="replace-string">
    <xsl:param name="text"/>
    <xsl:param name="replace"/>
    <xsl:param name="with"/>
	<xsl:variable name="tmp"><xsl:value-of select="translate($text, $replace,$with)"/></xsl:variable>
	<xsl:variable name="tmp2"><xsl:value-of select="translate($tmp, $replace,$with)"/></xsl:variable>
	<xsl:value-of select="translate($tmp2, $replace,$with)"/>
  </xsl:template>

<xsl:template match="text()|@*"> </xsl:template> 



</xsl:stylesheet> 
