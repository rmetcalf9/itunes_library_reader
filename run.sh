#!/bin/bash

SB_HOME=$(pwd)
##JAVA_HOME=C:\Program Files\Java\jdk1.8.0_05
CLASSPATH="$(pwd)/lib/xmlparserv2.jar:$(pwd)/lib/orai18n-collation.jar"

echo "Set classpath to ${CLASSPATH}"

function del_f {
	if [ -f ${1} ]
	then
		rm ${1}
	fi
}

function xform {
	del_f ${3}

	#1 = source
	#2 = xsl
	#3 = target
	T_CMD="${JAVA_CMD} -cp \"${CLASSPATH}\" oracle.xml.parser.v2.oraxsl \"${1}\" \"${2}\""
	echo "CMD=${T_CMD}"
	##T_RES2=$(${T_CMD} > "${3}")
	eval ${T_CMD} > "${3}"
	T_RES=$?
	if [[ ${T_RES} -ne 0 ]]
	then
		echo "Error in transformation"
		exit 1
	fi
}


JAVA_CMD=${JAVA_HOME}/bin/java
if [ E${JAVA_HOME} == "E" ]
then
	echo "JAVA_HOME enviroment variable is blank"
	JAVA_CMD="java"
fi

if [ ! -d ${SB_HOME}/output ]
then
	mkdir -p ${SB_HOME}/output
fi

##xform "${SB_HOME}/input/iTunes Music Library.xml" "${SB_HOME}/get_key_list.xsl" "${SB_HOME}/output/get_key_list.xml"

#::**Key list was too big to reduce
##xform "${SB_HOME}/output/get_key_list.xml" "${SB_HOME}/reduce_key_list.xsl" "${SB_HOME}/output/reduce_key_list.xml"


#::Get initial list of tracks - includes criteria to reduce volume
xform "${SB_HOME}/input/iTunes Music Library.xml" "${SB_HOME}/get_audio_track_list.xsl" "${SB_HOME}/output/get_audio_track_list.xml"

#::Create a list of just albums from the audio track list
xform "${SB_HOME}/output/get_audio_track_list.xml" "${SB_HOME}/reduce_to_album_list.xsl" "${SB_HOME}/output/reduce_to_album_list.xml"

#::Create html list of just albums from the audio track list
xform "${SB_HOME}/output/reduce_to_album_list.xml" "${SB_HOME}/make_html.xsl" "${SB_HOME}/output/album_list.html"

echo "Complete"
