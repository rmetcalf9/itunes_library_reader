<?xml version="1.0" encoding="UTF-8"?>
<!-- Given key list - reduce it for unique values -->
<xsl:stylesheet 
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"

	exclude-result-prefixes="xmlns"
>

	<xsl:key name="kForDedup" match="Row" use="concat(Kind,Year,Album_Artist,Album)"/>

	 <xsl:template match=
  "Row[not(generate-id() = generate-id(key('kForDedup', concat(Kind,Year,Album_Artist,Album))[last()]))]"
  />  

	<!-- By default ALL fields from the first album that matches key are output. 
	This template stops outputting the fields that are not common to all tracks in the album-->
	<xsl:template match="Row/*">
		<xsl:choose>
			<xsl:when test="local-name()='Kind'">
				<xsl:copy-of select="."/>
			</xsl:when>
			<xsl:when test="local-name()='Year'">
				<xsl:copy-of select="."/>
			</xsl:when>
			<xsl:when test="local-name()='Album_Artist'">
				<xsl:copy-of select="."/>
			</xsl:when>
			<!-- Artist should be left out but album artist is sometimes blank. 
			Artist may be different between tracks though so results may be misleading -->
			<xsl:when test="local-name()='Artist'">
				<xsl:copy-of select="."/>
			</xsl:when>
			<xsl:when test="local-name()='Album'">
				<xsl:copy-of select="."/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
  
  <xsl:template match="@*|*|processing-instruction()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()"/>
    </xsl:copy>

<xsl:text>
</xsl:text>

  </xsl:template>



</xsl:stylesheet> 
