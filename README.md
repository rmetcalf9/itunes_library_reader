itunes Library Reader
=====================

This project allows you to take an itunes library xml file and convert it into an xml format that can be simply imported into Excel or other simular applications

Pre-requisites
--------------

Check git is installed in your machine:
<code>
git version
</code>
Check java is installed in your machine:
<code>
java –version
</code>


Usage
-----

1. Clone this repo
2. Replace /input/iTunes Music Library.xml with your own version
3. Execute run.bat (windows) or run.sh (linux)
4. Output files are generated and stored in /output/ directory (index.html)


An example album list is https://rmetcalf9.gitlab.io/itunes_library_reader

