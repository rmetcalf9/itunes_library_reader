<?xml version="1.0" encoding="UTF-8"?>
<!-- Given key list - reduce it for unique values -->
<xsl:stylesheet 
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"

	exclude-result-prefixes="xmlns"
>

<!--<xsl:output method="text" indent="no" omit-xml-declaration="yes" />-->


	<xsl:key name="kForDedup" match="Key" use="text()"/>

	 <xsl:template match=
  "Key[not(generate-id() = generate-id(key('kForDedup', text())[last()]))]"
  />  

  <xsl:template match="@*|*|processing-instruction()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()"/>
    </xsl:copy>

<xsl:text>
</xsl:text>

  </xsl:template>



</xsl:stylesheet> 
