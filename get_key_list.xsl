<?xml version="1.0" encoding="UTF-8"?>
<!-- Gest list of keys against tracks-->
<xsl:stylesheet 
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"

	exclude-result-prefixes="xmlns"
>

<!--<xsl:output method="text" indent="no" omit-xml-declaration="yes" />-->

<xsl:template match="/">

	<!-- Make sure we look only at the dict element following the tracks key -->
	<xsl:for-each select="/plist/dict/key[text()='Tracks']/following-sibling::dict[1]">

		<xsl:element name="Keys">
			<xsl:for-each select="./dict/key">
					<xsl:element name="Key"><xsl:value-of select="."/></xsl:element>
			</xsl:for-each>
		</xsl:element>
	

	</xsl:for-each>
<xsl:text>
</xsl:text>

</xsl:template>

<xsl:template match="text()|@*"> </xsl:template> 



</xsl:stylesheet> 
